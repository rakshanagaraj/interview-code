# DO NOT USE THIS FILE. YOU MAY COPY ITS CONTENT INTO ANOTHER SCRIPT.

"""
    There are a few libs imported here, their use is only optional
    you may import other libs as well but you need to provide
    a requirements.txt if you use any third-party libs.
"""

import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import requests
import json
import sqlite3

def generate_password(length, complexity): 
    """Generate a random password with given length and complexity"""
 
    password= "" 
    msg= "Empty population for random generation"         
    
    
    ##If required complexity 1
    if complexity == 1:
        
        try:
            
            for i  in range(0, length):
            
                password = password + (random.choice(string.ascii_lowercase)) 
            
            return password
        
        except IndexError:
            
            print("Index Error %s." % msg)
    
    
    ##If required complexity 2
    elif complexity == 2: 
        
        
        try:    
            
            for i in range(0, length):
            
                password =  password + (random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits))
            
            return password
        
        except IndexError:
             
               print("Index Error %s." % msg)
                   
      
    ##If required complexity 3
    elif complexity == 3: 
        
         #Have a population for random generation
        try:    
            
          for i in range(0, length):
          
            password =  password + (random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits))
            
          return password
      
        except IndexError:
         
            print("Index Error %s." % msg)
   
   
    ##If required complexity 4
    elif complexity == 4: 
        
 
        try:    
          
          for  i in range(0, length):
          
            password =  password + (random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits +string.punctuation))
          
          return password
         
        except IndexError:
         
             print("Index Error %s." % msg)
         
  


def check_password_level(password):
    """Return the password complexity level for a given password"""


      
    if password.islower():
    
         return 1

    elif length == 2 and password.islower():
    
         return 2
      
       
    ## check if any character is digit 
    
    elif   any(char.isdigit()  for char in password) and any(char.islower()  for char in password):
        
            ##check also upper case 
            
               if   any(char.isupper()  for char in password):
                   
                   
                      ##check if punctuation
                      if   any(char.punctuation()  for char in password):
               
                                   
                                ##Password has only lowercase, uppercase, digits, and punctuation   
                                return 4 
                            
                      else:
                      
                                ##Password has only lowercase, uppercase, and digits
                                return 3 
               
               else:
                   
                   ##Password has only lowercase and digits 
                   ##Override condition password has length >= 8 chars and only lowercase and digits
 
                   if length >= 8:
                       
                         return 3
                   
                   return 2
               
               
    else: 
        
        return -1
               
               

               
               
def update(idno, fullname, email, password, dbpath):

    sql = ''' UPDATE information 
                    SET id= %d, 
                        name= %s, 
                        email= %s, 
                        password= %s''' %(idno, fullname, email, password)
                        
    #Exexute the query                    
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute(sql)
    conn.commit()
    conn.close()
  
            
def retrieve_info():

    r =  requests.get('https://randomuser.me/api/')
    information = r.json()
    fullname = information['results'][0]['name']['first'] + information['results'][0]['name']['last']
    email = information['results'][0]['email'] 
    
    retlist = [fullname, email]
    return retlist
               
               

def create_user(db_path):  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db"""
    ##Retrieve name and email 
 
    
    ##Connect to database 
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    ##Check if table exists 
    query =  '''SELECT id from information'''
    
    
    if not c.fetchone():
       
        ##Doeesn't exist so create one 
        c.execute('''CREATE TABLE information
             (id, name, emailid, username, password)''')
        conn.commit() 
        
     ##Get id 
    conn.close()
    
    ###update the database
   
     
    
    
   
       
    


   
    
    
    
    
    
    
    
    


     
